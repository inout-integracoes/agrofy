package agrofy;

import org.json.*;

import lib.omie.OmieClient;
import lib.salesforce.SalesforceClient;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inout.engine.filainout.*;
import com.inout.engine.route.*;
import com.inout.engine.route.properties.GlobalProperties;
import com.inout.server.type.FilaInoutStatus;
import com.inout.util.data.DateUtil;
import com.inout.util.db.DBPostgreSQL;
import com.inout.util.db.DataBase;

public class wsSalesforceToOmie_CasesEndpointB extends Script {
    private JSONObject properties = null;
    private OmieClient omie = null;
    private SalesforceClient sf = null;
    private DataBase dbPostgres = null;

    public void run() throws Exception {
        List<FilaInout> list = getRecords();
        properties = GlobalProperties.getPropertiesAsJSON("agrofy", "");

        String app_key = properties.optString("APP_KEY");
        String app_secret = properties.optString("APP_SECRET");

        omie = new OmieClient(app_key, app_secret, logger());
        sf = new SalesforceClient(logger());

        // sendBillingToSf("5007g000008jwHjAAI", 4454474088L);

        for (Object obj: list) {

            FilaInout filaInout = (FilaInout)obj;
            JSONObject record = filaInout.getRecord();

            try {
                if (record.has("json")) {
                    processPendingInstallment(record);
                } else {
                    processNewRecord(record);
                }

                filaInout.setStatusCode(FilaInoutStatus.SENT);
                filaInout.setStatusDesc("");
            } catch (Exception e) {
                filaInout.setStatusCode(FilaInoutStatus.TRANSFORMATION_ERROR);
                filaInout.setStatusDesc(e.getMessage());
                logError(e);
            }
        }
    }

    // Processa novo caso de faturamento
    private void processNewRecord(JSONObject record) throws Exception {
        for (Object casoObj : record.optJSONArray("billingCases")) {
            JSONObject caso = (JSONObject) casoObj;

            // Processamento do cabecalho
            JSONObject body = new JSONObject();
            JSONObject header = new JSONObject();
            JSONObject additionalInfo = new JSONObject();
            JSONObject email = new JSONObject();
            JSONArray services = new JSONArray();

            Date vencimento = DateUtil.stringToDate(caso.optString("dataPagamento"), "yyyy-MM-dd"); 

            String regex = "\\d+$";
            String subject = caso.optString("subject");

            Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(subject);

            int installmentNumber = 1;

            while (matcher.find()) {
                try {
                    installmentNumber = Integer.parseInt(matcher.group(0));
                } catch (Exception e) {
                    logInfo(String.format("Parcela nao encontrada no subject %s; assumindo 1...", subject));
                }
            }

            header.put("cCodIntCli", record.optString("salesforceAccountId"));
            header.put("nQtdeParc", record.getInt("billingFees"));
            header.put("cEtapa", "50"); // Fixo: 50 (Faturar)
    
            email.put("cEnvRecibo", "N");
            email.put("cEnvLink", "S");
            email.put("cEnvBoleto", "S");
            email.put("cEnviarPara", generateEmails(record));
    
            for (Object productObj : record.optJSONArray("product")) {
                JSONObject product = (JSONObject) productObj;
                JSONObject service = new JSONObject();
                
                if (product.optString("name").equalsIgnoreCase("Desarrollo tecnológico")) {
                    service.put("cCodServMun", "06298");
                    service.put("cTribServ", "01");
                    service.put("cCodServLC116", "10.05");
                    service.put("cRetemISS", "N");
    
                    JSONObject taxes = new JSONObject();
                    taxes.put("cRetemIRRF", "S");
    
                    if (product.optDouble("amount") > 666.67) {
                        taxes.put("nAliqIRRF", 1.5);
                    }
    
                    service.put("impostos", taxes);
                } else {
                    service.put("cCodServMun", "02498");
                    service.put("cTribServ", "01");
                    service.put("cCodServLC116", "17.24");
                    service.put("cRetemISS", "N");
    
                    JSONObject taxes = new JSONObject();
                    taxes.put("nAliqISS", 2.9);
                    taxes.put("cRetemIRRF", "S");
    
                    service.put("impostos", taxes);
                }
                
                service.put("nValUnit", caso.optString("importeBoleto"));
                service.put("nQtde", product.optDouble("quantidade"));
                service.put("nValorDesconto", product.optDouble("discountAmount"));
                service.put("cDescServ", generateAdditionalInfo(record, vencimento, installmentNumber));
                services.put(service);
            }

            // Processamento de parcela
            JSONArray installments = new JSONArray();
            JSONObject installment = new JSONObject();

            String targetDate = caso.optString("Fecha_Facturacion__c");
            Date insertionDate = DateUtil.stringToDate(targetDate, "yyyy-MM-dd");
            
            String caseId = caso.optString("caseId");
            String saleId = record.optString("saleId") + "_" + installmentNumber;

            additionalInfo.put("cCodCateg", "1.01.02"); // Fixo: Clientes - Serviços Prestados
            additionalInfo.put("nCodCC", 3690387108L); // Fixo: Santander
            additionalInfo.put("cNumPedido", record.optString("saleId"));
            additionalInfo.put("cCidPrestServ", "SAO PAULO (SP)");
            additionalInfo.put("dDataRps", processRPSDate(insertionDate));

            header.put("cCodIntOS", saleId);
            header.put("cNumOS", saleId);
            header.put("dDtPrevisao", DateUtil.dateToString(insertionDate, "dd/MM/yyyy"));
            
            installment.put("nParcela", installmentNumber);
            installment.put("dDtVenc", DateUtil.dateToString(vencimento, "dd/MM/yyyy"));
            installment.put("nValor", caso.optString("importeBoleto"));
            installment.put("nPercentual", 100);

            // Os valores fixos abaixo estao descritos em https://app.omie.com.br/api/v1/servicos/os/#ParcelasArray
            if (record.optString("paymentForm").equalsIgnoreCase("Boleto bancário")) {
                installment.put("tipo_documento", "BOL");
                installment.put("meio_pagamento", "15");
            } else if (record.optString("paymentForm").equalsIgnoreCase("Transferencia bancaria")) {
                installment.put("tipo_documento", "TRA");
                installment.put("meio_pagamento", "18");
                installment.put("nao_gerar_boleto", "S");
            } else if (record.optString("paymentForm").equalsIgnoreCase("Cartão de crédito")) {
                installment.put("tipo_documento", "CRC");
                installment.put("meio_pagamento", "03");
                installment.put("nao_gerar_boleto", "S");
            }

            installments.put(installment);

            JSONObject observation = new JSONObject();
            observation.put("cObsOS", String.format("Cliente: %s", record.optString("internalClientId")));

            body.put("Cabecalho", header);
            body.put("InformacoesAdicionais", additionalInfo);
            body.put("Email", email);
            body.put("ServicosPrestados", services);
            body.put("Parcelas", installments);
            body.put("Observacoes", observation);

            addPendingCase(saleId, caseId, insertionDate, body);
        }
    }

    // Envia a OS ja processada armazenada no banco e fatura
    private void processPendingInstallment(JSONObject record) throws Exception {
        JSONObject pendingInstallment = new JSONObject(record.getString("json"));

        String caseId = record.getString("caseId");
        String saleId = record.getString("saleId");

        JSONObject response = omie.postOS(pendingInstallment).optJSONObject("response");
        long nCodOS = response.optLong("nCodOS");
        updateCaseState(saleId, caseId, nCodOS, true, false); // marca como enviado

        omie.billOS(saleId); // Invoca rotina de faturamento no Omie
        updateCaseState(saleId, caseId, nCodOS, true, true); // marca como enviado e faturado

        // Precisamos de um delay para dar tempo de gerar a NFSe. Se nao esperarmos esse tempo,
        // a consulta do omie.getNFSe pode retornar sem dados criticos da NFSe.
        Thread.sleep(60 * 1000);
        sendBillingToSf(caseId, nCodOS);
    }

    // Consulta a NFSe no Omie e gera o JSON a ser retornado para o Salesforce
    private void sendBillingToSf(String caseId, long nCodOS) throws Exception {
        // print parameters concatenated
        logInfo(String.format("Sending billing to Salesforce for case %s with code %d", caseId, nCodOS));

        JSONObject omieResponse = omie.getNFSe(nCodOS);

        if (!omieResponse.optBoolean("success")) {
            throw new Exception("Faturamento do caso " + caseId + " nao encontrado no Omie!");
        }

        JSONObject response = omieResponse.optJSONObject("response");
        JSONObject billing = (JSONObject) response.optJSONArray("nfseEncontradas").get(0);

        JSONObject header = billing.optJSONObject("Cabecalho");
        JSONObject issuance = billing.optJSONObject("Emissao");

        String cIMEmissor = header.optString("cIMEmissor");
        String nNumeroNFSe = header.optString("nNumeroNFSe");
        String cCodigoVerifNFSe = header.optString("cCodigoVerifNFSe");

        String linkNFSe = String.format("https://nfe.prefeitura.sp.gov.br/contribuinte/notaprint.aspx?ccm=%s&nf=%s&cod=%s",
                                        cIMEmissor, nNumeroNFSe, cCodigoVerifNFSe);

        JSONObject caseBody = new JSONObject();
        caseBody.put("Numero_da_NF__c", header.optString("nNumeroNFSe"));
        caseBody.put("Fecha_Facturacion__c", DateUtil.dateToString(DateUtil.stringToDate(issuance.optString("cDataEmissao"), "dd/MM/yyyy"), "yyyy-MM-dd"));
        caseBody.put("Link_da_NF__c", linkNFSe);

        // logInfo("Billing que seria enviado: " + caseBody.toString());
        sf.updateCase(caseId, caseBody);
    }

    // Adiciona o email fixo e o email do cliente no caso, separado por vírgula
    private String generateEmails(JSONObject record) {
        String result = "financeiro.br@agrofy.com";

        JSONObject billingContact = record.getJSONObject("billingContact");
        if (!billingContact.optString("email").isEmpty()) {
            result += ", " + billingContact.optString("email");
        }

        return result;
    }

    // Se o dia atual for 1, 2 ou 3, a data da RPS deve ser o ultimo dia do mes anterior
    private String processRPSDate(Date targetDate) {
        Calendar today = Calendar.getInstance();
        int dayOfMonth = today.get(Calendar.DAY_OF_MONTH);

        if (dayOfMonth <= 3) {
            Calendar lastDayOfLastMonth = Calendar.getInstance();
            lastDayOfLastMonth.add(Calendar.MONTH, -1);
            lastDayOfLastMonth.set(Calendar.DAY_OF_MONTH, lastDayOfLastMonth.getActualMaximum(Calendar.DAY_OF_MONTH));

            return DateUtil.dateToString(lastDayOfLastMonth.getTime(), "dd/MM/yyyy");
        }

        return null;
    }

    // Gera o texto adicional da NFS-e
    private String generateAdditionalInfo(JSONObject record, Date dueDate, int installmentNumber) {
        String result = "";

        if (!record.optString("descriçãoNotaFiscal").isEmpty()) {
            result += record.optString("descriçãoNotaFiscal") + "\n";
        }

        for (Object productObj : record.optJSONArray("product")) {
            JSONObject product = (JSONObject) productObj;
            result += String.format("Serviço: %s\n", product.optString("name"));
        }

        result += String.format("Parcela: %d\n", installmentNumber);
        result += String.format("Data de vencimento: %s\n", DateUtil.dateToString(dueDate, "dd/MM/yyyy"));

        if (record.optString("paymentForm").equalsIgnoreCase("Transferencia bancaria")) {
            result += "\nBanco Santander Ag: 2050 - C/c: 13004357-0";
        }

        return result;
    }

    // Atualiza o estado do caso no banco, marcando o numero da OS gerada, se a OS foi enviada e se a OS foi faturada
    private boolean updateCaseState(String saleId, String caseId, long nCodOS, boolean sent, boolean billed) throws Exception{       
        dbPostgres = null;
        try {
            dbPostgres = new DBPostgreSQL("localhost", "5432", "postgres", "postgres", "postgres");
        } catch (Exception eConnPostgre) {
            throw new Exception("Error connect Postgres.", eConnPostgre);
        }
        if (dbPostgres == null || !dbPostgres.connectionIsValid()) {
            throw new Exception("connectionIsValid Postgres.");
        }

        try {
            dbPostgres.update("agrofy_cases")
                      .fields(new String[] {"sent", "billed", "nCodOS"})
                      .where("saleId = ? and caseId = ?")
                      .param(sent)
                      .param(billed)
                      .param(String.valueOf(nCodOS))
                      .param(saleId)
                      .param(caseId)
                      .execute();

            return true;
        } catch (Exception e) {
            logError(String.format("Erro ao atualizar state do caso %s da OS %s", caseId, saleId));
            logError(e);
            
            return false;
        } finally {
            dbPostgres.closeConnection();
        }
    }

    // Adiciona o caso ja processado no banco. Quando chegar no dia do insertionDate, basta enviar o json.
    private boolean addPendingCase(String saleId, String caseId, Date insertionDate, JSONObject billingCase) throws Exception {        
        dbPostgres = null;
        try {
            dbPostgres = new DBPostgreSQL("localhost", "5432", "postgres", "postgres", "postgres");
        } catch (Exception eConnPostgre) {
            throw new Exception("Error connect Postgres.", eConnPostgre);
        }
        if (dbPostgres == null || !dbPostgres.connectionIsValid()) {
            throw new Exception("connectionIsValid Postgres.");
        }

        try {
            // Upsert garante que nao teremos problema caso alterem algum dado de parcela
            String sql = "INSERT INTO agrofy_cases VALUES(?, ?, ?, ?, ?, ?, ?) ";
            sql += "    ON CONFLICT (saleId, caseId) DO UPDATE ";
            sql += "    SET json = EXCLUDED.json, send_date = EXCLUDED.send_date, sent = EXCLUDED.sent, ";
            sql += "    billed = EXCLUDED.billed, nCodOS = EXCLUDED.nCodOS";

            dbPostgres.sql(sql)
                      .param(saleId)
                      .param(caseId)
                      .param(null)
                      .param(billingCase.toString())
                      .param(Timestamp.from(insertionDate.toInstant()))
                      .param(false)
                      .param(false)
                      .execute();
                
            logInfo(String.format("caseId %s inserido no banco para insercao em %s", caseId, DateUtil.dateToString(insertionDate, "dd/MM/yyyy")));

            return true;
        } catch (Exception e) {
            logError(String.format("Erro ao salvar caso %s da OS %s", caseId, saleId));
            logError(e);
            
            return false;
        } finally {
            dbPostgres.closeConnection();
        }
    }
}
