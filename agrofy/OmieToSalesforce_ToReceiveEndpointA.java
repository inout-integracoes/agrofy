package agrofy;

import org.json.*;

import lib.omie.OmieClient;

import com.inout.engine.route.*;
import com.inout.engine.route.properties.GlobalProperties;
import com.inout.util.data.DateUtil;

public class OmieToSalesforce_ToReceiveEndpointA extends Script {
    private JSONObject properties = null;
    private OmieClient omie = null;

    @Override
    public void run() throws Exception {
        properties = GlobalProperties.getPropertiesAsJSON("agrofy", "");

        String app_key = properties.optString("APP_KEY");
        String app_secret = properties.optString("APP_SECRET");

        omie = new OmieClient(app_key, app_secret, logger());

        String yesterday = DateUtil.dateToString(DateUtil.getDateBeforeToday(1), "dd/MM/yyyy"); 
        JSONObject result = omie.getBillingsToReceiveByDate(yesterday);
        
        if (result.optBoolean("success")) {
            JSONObject response = result.optJSONObject("response");
            JSONArray billings = response.optJSONArray("conta_receber_cadastro");

            for (Object billing : billings) {
                JSONObject bill = (JSONObject) billing;
                addNewRecord(bill);
            }
        }
    }
}

