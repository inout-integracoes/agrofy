package agrofy;

import org.json.*;

import lib.omie.OmieClient;
import lib.salesforce.SalesforceClient;

import java.util.*;
import com.inout.engine.filainout.*;
import com.inout.engine.route.*;
import com.inout.engine.route.properties.GlobalProperties;
import com.inout.server.type.*;
import com.inout.util.db.DBPostgreSQL;
import com.inout.util.db.DataBase;

public class OmieToSalesforce_ToReceiveEndpointB extends Script {
    private JSONObject properties = null;
    private OmieClient omie = null;
    private SalesforceClient sf = null;

    public void run() throws Exception {
        properties = GlobalProperties.getPropertiesAsJSON("agrofy", "");

        String app_key = properties.optString("APP_KEY");
        String app_secret = properties.optString("APP_SECRET");

        omie = new OmieClient(app_key, app_secret, logger());
        sf = new SalesforceClient(logger());
        
        List<FilaInout> list = getRecords();

        for (Object obj: list) {

            FilaInout filaInout = (FilaInout)obj;
            JSONObject record = filaInout.getRecord();
            try {
                processBillToReceive(record);

                filaInout.setStatusCode(FilaInoutStatus.SENT);
                filaInout.setStatusDesc("");

            } catch (Exception e) {
                filaInout.setStatusCode(FilaInoutStatus.TRANSFORMATION_ERROR);
                filaInout.setStatusDesc(e.getMessage());
                logError(e);
            }
        }
    }

    private void processBillToReceive(JSONObject record) throws Exception {
        JSONObject boleto = record.optJSONObject("boleto");
        long nCodOS = record.optLong("nCodOS");
        String caseId = getCaseIdBynCodOS(nCodOS);

        JSONObject title = omie.getTitle(String.valueOf(nCodOS));
        JSONObject cabecTitulo = title.optJSONObject("cabecTitulo");
        JSONObject resumo = title.optJSONObject("resumo");

        JSONObject caseBody = new JSONObject();
        caseBody.put("Numero_da_Boleto__c", boleto.optString("cNumBoleto"));
        caseBody.put("C_digo_de_barras__c", cabecTitulo.optString("cCodigoBarras"));
        caseBody.put("Valor_recibido__c", resumo.optString("nValPago"));

        if (resumo.optString("cLiquidado").equals("S")) {
            caseBody.put("Check_Pago__c", "true");
        }
        
        sf.updateCase(caseId, caseBody);
    }

    private String getCaseIdBynCodOS(long nCodOS) throws Exception {
        DataBase dbPostgres = null;
        try {
            dbPostgres = new DBPostgreSQL("localhost", "5432", "postgres", "postgres", "postgres");
        } catch (Exception eConnPostgre) {
            throw new Exception("Error connect Postgres.", eConnPostgre);
        }
        if (dbPostgres == null || !dbPostgres.connectionIsValid()) {
            throw new Exception("connectionIsValid Postgres.");
        }

        try {

            String sql = "SELECT saleId, caseId FROM agrofy_cases WHERE nCodOS = ?";
            JSONObject result = dbPostgres.sql(sql)
                               .fields(new String[] {"saleId", "caseId"})
                               .param(String.valueOf(nCodOS))
                               .queryUnique();

            return result.optString("caseId");
        } catch (Exception e) {
            logInfo("Erro ao pegar parcelas pendentes");
            logError(e);

            return null;
        } finally {
            dbPostgres.closeConnection();
        }
    }
}

