package agrofy;

import org.json.*;

import java.sql.Timestamp;
import java.util.*;

import com.inout.engine.*;
import com.inout.engine.route.*;
import com.inout.util.db.DBPostgreSQL;
import com.inout.util.db.DataBase;
import com.inout.util.data.DateUtil;

public class wsSalesforceToOmie_CasesEndpointA extends Script {
	private JSONObject input = new JSONObject();

    @Override
    public void run() throws Exception {
        Route route = RouteEngine.getRoute(routeId());
        input = route.getEndpointA().getInputData();

        createMainTable();

        // Se o input nao for nulo, recebemos info via ws.
        if (input != null) {
            logInfo("Recebido do Salesforce: " + input.toString());

            if (input.getInt("billingFees") == input.optJSONArray("billingCases").length()) {
                addNewRecord(input);
                route.getEndpointB().setOutputData(input);
            } else {
                JSONObject response = new JSONObject();
                response.put("success", false);
                response.put("message", "billingFees and billingCases' length are not the same!");
                route.getEndpointB().setOutputData(response);
            }

        } else {
            JSONObject response = new JSONObject();
            response.put("success", false);
            route.getEndpointB().setOutputData(response);
        }

        // Sempre verificamos a cada execucao.
        getPendingInstallments();
    }

    // Consulta a tabela temporaria procurando por parcelas que devem ser enviadas ao Omie hoje.
    // Caso encontre, coloca na fila para processamento.
    private void getPendingInstallments() throws Exception {
        DataBase dbPostgres = null;
        try {
            dbPostgres = new DBPostgreSQL("localhost", "5432", "postgres", "postgres", "postgres");
        } catch (Exception eConnPostgre) {
            throw new Exception("Error connect Postgres.", eConnPostgre);
        }
        if (dbPostgres == null || !dbPostgres.connectionIsValid()) {
            throw new Exception("connectionIsValid Postgres.");
        }

        try {
            Date today = DateUtil.getToday();

            String sql = "SELECT saleId, caseId, json FROM agrofy_cases WHERE send_date <= ? AND NOT sent";
            JSONArray result = dbPostgres.sql(sql)
                               .fields(new String[] {"saleId", "caseId", "json"})
                               .param(Timestamp.from(today.toInstant()))
                               .query();

            for (Object pendingCaseObj : result) {
                JSONObject pendingCase = (JSONObject) pendingCaseObj;

                addNewRecord(pendingCase);
            }
        } catch (Exception e) {
            logInfo("Erro ao pegar parcelas pendentes");
            logError(e);
        } finally {
            dbPostgres.closeConnection();
        }
    }

    private void createMainTable() throws Exception {
        DataBase dbPostgres = null;
        try {
            dbPostgres = new DBPostgreSQL("localhost", "5432", "postgres", "postgres", "postgres");
        } catch (Exception eConnPostgre) {
            throw new Exception("Error connect Postgres.", eConnPostgre);
        }
        if (dbPostgres == null || !dbPostgres.connectionIsValid()) {
            throw new Exception("connectionIsValid Postgres.");
        }

        // Tabela que armazena casos de faturamento para envio posterior
        String sql = "CREATE TABLE IF NOT EXISTS agrofy_cases (";
        sql += "    saleId VARCHAR(25) NOT NULL,";
        sql += "    caseId VARCHAR(25) NOT NULL,";
        sql += "    nCodOS VARCHAR(25) NULL,";
        sql += "    json text  NOT NULL,";
        sql += "    send_date date NOT NULL,";
        sql += "    sent boolean NOT NULL DEFAULT FALSE,";
        sql += "    billed boolean NOT NULL DEFAULT FALSE,";
        sql += "    CONSTRAINT pk_agrofy_cases PRIMARY KEY (saleId, caseId)";
        sql += ");";

        try {
            dbPostgres.sql(sql).execute();
        } catch (Exception e) {
            logError("Erro ao criar tabela: ");
            logError(e);
        }
    }
}