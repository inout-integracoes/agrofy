package agrofy;

import org.json.*;
import com.inout.engine.*;
import com.inout.engine.route.*;

public class wsSalesforceToOmie_ClientsEndpointA extends Script {
	private JSONObject input = new JSONObject();

    @Override
    public void run() throws Exception {
        Route route = RouteEngine.getRoute(routeId());
        
        input = route.getEndpointA().getInputData();
        
        String tipoNIF = input.getString("tipoNIF");
        boolean isValid = tipoNIF.equalsIgnoreCase("cpf") || tipoNIF.equalsIgnoreCase("cnpj");
        
        if (isValid) {
            addNewRecord(input);
            route.getEndpointB().setOutputData(input);
        } else {
            JSONObject response = new JSONObject();
            response.put("success", false);
            response.put("reason", "Invalid tipoNIF: must be CPF or CNPJ");
            route.getEndpointB().setOutputData(response);
        }
    }
}
