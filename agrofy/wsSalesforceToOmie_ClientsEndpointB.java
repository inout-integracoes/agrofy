package agrofy;

import org.json.*;

import lib.omie.OmieClient;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inout.engine.filainout.*;
import com.inout.engine.route.*;
import com.inout.engine.route.properties.GlobalProperties;
import com.inout.server.type.*;

public class wsSalesforceToOmie_ClientsEndpointB extends Script {
    private JSONObject properties = null;
    private OmieClient omie = null;

    public void run() throws Exception {
        List<FilaInout> list = getRecords();
        properties = GlobalProperties.getPropertiesAsJSON("agrofy", "");

        String app_key = properties.optString("APP_KEY");
        String app_secret = properties.optString("APP_SECRET");

        omie = new OmieClient(app_key, app_secret, logger());

        // Percorre a lista de objetos
        for (Object obj: list) {

            FilaInout filaInout = (FilaInout)obj;
            JSONObject record = filaInout.getRecord();
            try {
                JSONObject tag = new JSONObject();
                JSONArray tags = new JSONArray();
                JSONObject client = new JSONObject();

                String endereco = record.optString("street");
                Pattern pattern = Pattern.compile("(.*), (.*) ?\\/ ?(.*)", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(endereco);
                boolean matchFound = matcher.find();

                if (matchFound) {
                    client.put("endereco", matcher.group(1));
                    client.put("endereco_numero", matcher.group(2));
                    client.put("bairro", matcher.group(3));
                } else {
                    logInfo("Nao foi possivel gerar endereco com Regex; seguindo padrao de antes...");
                    client.put("endereco", record.optString("street"));
                }

                client.put("cep", record.optString("postalCode"));
                client.put("cidade", record.optString("city"));
                client.put("estado", getSiglaEstado(record.optString("province")));

                client.put("razao_social", record.optString("nameAccount"));
                client.put("cnpj_cpf", record.optString("nif"));
                client.put("email", record.optString("emailAccount"));
                client.put("codigo_cliente_integracao", record.getString("salesforceId"));

                tag.put("tag", "Cliente");
                tags.put(tag);

                client.put("tags", tags);

                logInfo("client gerado: " + client.toString());
                JSONObject response = omie.postClient(client);
                logInfo("sendClient resposta: " + response.toString());

                filaInout.setStatusCode(FilaInoutStatus.SENT);
                filaInout.setStatusDesc("");

            } catch (Exception e) {
                filaInout.setStatusCode(FilaInoutStatus.TRANSFORMATION_ERROR);
                filaInout.setStatusDesc(e.getMessage());
                logError(e);
            }
        }
    }

    private String getSiglaEstado(String estado) {
        if (estado.equalsIgnoreCase("Acre")) {
            return "AC";
        }
        else if (estado.equalsIgnoreCase("Alagoas")) {
            return "AL";
        }
        else if (estado.equalsIgnoreCase("Amapá")) {
            return "AP";
        }
        else if (estado.equalsIgnoreCase("Amazonas")) {
            return "AM";
        }
        else if (estado.equalsIgnoreCase("Bahia")) {
            return "BA";
        }
        else if (estado.equalsIgnoreCase("Ceará")) {
            return "CE";
        }
        else if (estado.equalsIgnoreCase("Distrito Federal")) {
            return "DF";
        }
        else if (estado.equalsIgnoreCase("Espírito Santo")) {
            return "ES";
        }
        else if (estado.equalsIgnoreCase("Goiás")) {
            return "GO";
        }
        else if (estado.equalsIgnoreCase("Maranhão")) {
            return "MA";
        }
        else if (estado.equalsIgnoreCase("Mato Grosso")) {
            return "MT";
        }
        else if (estado.equalsIgnoreCase("Mato Grosso do Sul")) {
            return "MS";
        }
        else if (estado.equalsIgnoreCase("Minas Gerais")) {
            return "MG";
        }
        else if (estado.equalsIgnoreCase("Pará")) {
            return "PA";
        }
        else if (estado.equalsIgnoreCase("Paraíba")) {
            return "PB";
        }
        else if (estado.equalsIgnoreCase("Paraná")) {
            return "PR";
        }
        else if (estado.equalsIgnoreCase("Pernambuco")) {
            return "PE";
        }
        else if (estado.equalsIgnoreCase("Piauí")) {
            return "PI";
        }
        else if (estado.equalsIgnoreCase("Rio de Janeiro")) {
            return "RJ";
        }
        else if (estado.equalsIgnoreCase("Rio Grande do Norte")) {
            return "RN";
        }
        else if (estado.equalsIgnoreCase("Rio Grande do Sul")) {
            return "RS";
        }
        else if (estado.equalsIgnoreCase("Rondônia")) {
            return "RO";
        }
        else if (estado.equalsIgnoreCase("Roraima")) {
            return "RR";
        }
        else if (estado.equalsIgnoreCase("Santa Catarina")) {
            return "SC";
        }
        else if (estado.equalsIgnoreCase("São Paulo")) {
            return "SP";
        }
        else if (estado.equalsIgnoreCase("Sergipe")) {
            return "SE";
        }
        else if (estado.equalsIgnoreCase("Tocantins")) {
            return "TO";
        } else {
            return estado;
        }
    }
}

