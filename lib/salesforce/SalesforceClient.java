package lib.salesforce;

import com.inout.util.log.InoutLogger;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class SalesforceClient {
    private InoutLogger logger = null;
    private String accessToken = null;

    public SalesforceClient(InoutLogger logger) throws Exception {
        this.logger = logger;

        if (accessToken == null) {
            doAuth();
        }
    }

    private void doAuth() throws Exception {
        String url = "https://test.salesforce.com/services/oauth2/token";

        logger.logInfo("Iniciando processo de autenticacao ao Salesforce...");

        try {
            JSONObject result = makeRequest(url, "POST", null);

            if (result.optBoolean("success")) {
                JSONObject response = result.optJSONObject("response");
                accessToken = response.optString("access_token");
            } else {
                logger.logError("houve um erro ao autenticar: " + result.toString());
            }
        } catch (Exception e) {
            logger.logError(e);
        }
    }

    public JSONObject updateCase(String caseId, JSONObject caseBody) throws Exception {
        String url = "https://CS200.salesforce.com/services/data/v20.0/sobjects/Case/" + caseId;
        JSONObject result = makeRequest(url, "PATCH", caseBody);

        if (result.optBoolean("success")) {
            JSONObject response = result.optJSONObject("response");
            return response;
        } else {
            logger.logError("houve um erro ao atualizar o caso: " + result.toString());
            return null;
        }
    }

    private JSONObject makeRequest(String url, String metodo, JSONObject json) throws Exception {       
        boolean success = false;
        int count = 0;

        if (json != null) {
            logger.logInfo("Tentativa " + (count + 1) + " | URL: " + url + " | Payload " + json.toString());
        } else {
            logger.logInfo("Tentativa " + (count + 1) + " | URL: " + url);
        }

        while (!success && count++ < 5) {
            
            logger.logInfo(metodo + " url " + url);
    
            HttpClient httpclient = HttpClients.createDefault();
            
            HttpUriRequest request = null;
            RequestBuilder builder = null;
           
            if (metodo != null) {
                switch (metodo) {
                    case "GET":
                        builder = RequestBuilder.get();
                        break;
                    case "POST":
                        builder = RequestBuilder.post();
                        break;
                    case "PUT":
                        builder = RequestBuilder.put();
                        break;
                    case "DELETE":
                        builder = RequestBuilder.put();
                        break;
                    case "PATCH":
                        builder = RequestBuilder.patch();
                        break;
                    default:
                        break;
                }
            }
            
            // Se for autenticacao, enviamos texto fixo como x-www-form-urlencoded
            if (url.contains("oauth2")) {
                String text = "grant_type=password&client_id=3MVG99E3Ry5mh4zpLFq1EyXcKFywc.I_Yn.ALW9acMnI7yonyfC7sjMnYrdbUYr4Sxsue4i_df2bEaHfDg5qN";
                text += "&client_secret=2968260BEAB0D440A374CA4645A039199008AC99AD6719F05CA4743EC5E82165";
                text += "&username=integration%40inoutway.com.br&password=Inout2021TqhIsFgQ7rulyJ2CWGiCHr0y";

                StringEntity entity = new StringEntity(text, "UTF-8");
                entity.setContentType("application/x-www-form-urlencoded");
                builder = builder.setEntity(entity).setUri(url);
            } else {
                if (json != null) {
                    StringEntity entity = new StringEntity(json.toString(), "UTF-8");
                    entity.setContentType("application/json");
                    builder = builder.setEntity(entity).setUri(url);
                }
            }
            
            builder = builder.setUri(url);

            request = builder.build();
            
            if (accessToken == null) {
                request.addHeader("Content-Type", "application/x-www-form-urlencoded");
            } else {
                request.addHeader("Content-Type", "application/json");
                request.addHeader("Authorization", "Bearer " + accessToken);
            }
            
            HttpResponse response = httpclient.execute(request);
    
            int code = response.getStatusLine().getStatusCode();
            String bodyString = "";
            
            if (response.getEntity() != null) {
                bodyString = EntityUtils.toString(response.getEntity());
            }

            if (bodyString.isEmpty()) {
                bodyString = "{}";
            }
                
            logger.logInfo("Retorno: " + bodyString);
            logger.logInfo("Code: " + code);
            
            Object resposta = new JSONTokener(bodyString).nextValue();
            
            JSONObject retorno = new JSONObject();
            Header[] headers = response.getAllHeaders();
            JSONArray headers_arr = new JSONArray();
            
            if (code == 200 || code == 201 || code == 204) {
                count = 0;
                success = true;
            }
        
            for (Header header : headers) {
                JSONObject header_obj = new JSONObject();
                header_obj.put(header.getName(), header.getValue());
                headers_arr.put(header_obj);
            }
            
            if (code >= 300 && code != 429 && code != 404) {
                logger.logInfo(String.format(" >>> SALESFORCE: Erro executando [%s] em '%s' - Status Code=%s", metodo, url, code));
                logger.logInfo(String.format(" >>> SALESFORCE: Response Headers: "));
                
                for (Header header : headers) {
                    logger.logInfo(String.format("   > %s=%s", header.getName(), header.getValue()));
                }  
                
                logger.logInfo(String.format(" >>> SALESFORCE: Response Body: %s", bodyString));
                
                if (json != null)
                    logger.logInfo(String.format(" >>> SALESFORCE: Request Body: %s", json.toString()));
    
                if (bodyString.contains("erros")) {
                    JSONObject bodyStr = new JSONObject(bodyString);
                    throw new RuntimeException(String.format(" >>> SALESFORCE: %s: %s", response.getStatusLine().getReasonPhrase(), bodyStr.toString(2)));
                }
    
                throw new RuntimeException(String.format(" >>> SALESFORCE: Reason: %s", response.getStatusLine().getReasonPhrase()));
            }
        
            retorno.put("headers", headers_arr);
            retorno.put("response", resposta);
            retorno.put("code", code);
            retorno.put("success", (code == 200 || code == 201 || code == 204));
            
            success = true;
            
            return retorno;
        }
    
        return null;
    }
}
