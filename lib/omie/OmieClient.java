package lib.omie;

import com.inout.util.log.InoutLogger;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class OmieClient {
    private String appKey = null;
    private String appSecret = null;
    private InoutLogger logger = null;
    private String baseURL = "https://app.omie.com.br/api/v1/";

    public OmieClient(String appKey, String appSecret, InoutLogger logger) {
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.logger = logger;
    }

    public JSONObject postClient(JSONObject input) throws Exception {
        JSONObject client = new JSONObject();
        client.put("razao_social", input.optString("razao_social"));
        client.put("cnpj_cpf", input.optString("cnpj_cpf"));
        client.put("endereco", input.optString("endereco").trim());
        client.put("endereco_numero", input.optString("endereco_numero").trim());
        client.put("bairro", input.optString("bairro").trim());
        client.put("cep", input.optString("cep"));
        client.put("cidade", input.optString("cidade"));
        client.put("estado", input.optString("estado"));
        client.put("email", input.optString("email"));
        client.put("contato", input.optString("contato"));
        client.put("codigo_cliente_integracao", input.optString("codigo_cliente_integracao"));
        client.put("telefone1_numero", input.optString("telefone1_numero"));

        JSONObject response = makeRequest("geral/clientes/", "UpsertCliente", "POST", client);

        return response;
    }

    public JSONObject postOS(JSONObject input) throws Exception {
        JSONObject response = makeRequest("servicos/os/", "IncluirOS", "POST", input);

        return response;
    }

    public JSONObject updateOS(JSONObject input) throws Exception {
        JSONObject response = makeRequest("servicos/os/", "AlterarOS", "POST", input);

        return response;
    }

    public JSONObject getOS(String cCodIntOS) throws Exception {
        JSONObject params = new JSONObject();
        params.put("cCodIntOS", cCodIntOS);

        JSONObject response = makeRequest("servicos/os/", "ConsultarOS", "POST", params);

        return response;
    }

    public JSONObject billOS(String cCodIntOS) throws Exception {
        JSONObject params = new JSONObject();
        params.put("cCodIntOS", cCodIntOS);

        JSONObject response = makeRequest("servicos/osp/", "FaturarOS", "POST", params);

        return response;
    }

    public JSONObject getBillingsToReceiveByDate(String date) throws Exception {
        JSONObject params = new JSONObject();
        params.put("pagina", 1);
        params.put("registros_por_pagina", 50);
        params.put("filtrar_por_emissao_de", date);

        JSONObject response = makeRequest("financas/contareceber/", "ListarContasReceber", "POST", params);

        return response;
    }

    public JSONObject getTitle(String nCodOS) throws Exception {
        JSONObject params = new JSONObject();
        params.put("nPagina", 1);
        params.put("nRegPorPagina", 50);
        params.put("nCodOS", nCodOS);

        JSONObject response = makeRequest("financas/pesquisartitulos/", "PesquisarLancamentos", "POST", params);

        return response;
    }

    public JSONObject getURLBoleto(String nCodTitulo) throws Exception {
        JSONObject params = new JSONObject();
        params.put("nCodTitulo", nCodTitulo);

        JSONObject response = makeRequest("financas/pesquisartitulos/", "ObterURLBoleto", "POST", params);

        return response;
    }

    public JSONObject getNFSe(long nCodOS) throws Exception {
        JSONObject params = new JSONObject();
        params.put("nCodigoOS", nCodOS);

        JSONObject response = makeRequest("servicos/nfse/", "ListarNFSEs", "POST", params);

        return response;
    }

    private JSONObject makeRequest(String endpoint, String call, String metodo, JSONObject json) throws Exception {
        JSONArray param = new JSONArray();
        JSONObject body = new JSONObject();
        
        boolean success = false;
        int count = 0;

        param.put(json);

        body.put("call", call);
        body.put("app_key", appKey);
        body.put("app_secret", appSecret);
        body.put("param", param);

        logger.logInfo("Tentativa " + (count + 1) + " | Endpoint: " + endpoint + " | Payload " + body.toString());

        while (!success && count++ < 5) {
            String url = baseURL + endpoint;
            
            logger.logInfo(metodo + " url " + url);
    
            HttpClient httpclient = HttpClients.createDefault();
            
            HttpUriRequest request = null;
            RequestBuilder building = null;
           
            if (metodo != null) {
                switch (metodo) {
                    case "GET":
                        building = RequestBuilder.get();
                        break;
                    case "POST":
                        building = RequestBuilder.post();
                        break;
                    case "PUT":
                        building = RequestBuilder.put();
                        break;
                    case "DELETE":
                        building = RequestBuilder.put();
                        break;
                    default:
                        break;
                }
            }
            
            if (body != null) {
                StringEntity entity = new StringEntity(body.toString(), "UTF-8");
                entity.setContentType("application/json");
                building = building.setEntity(entity).setUri(url);
            }
            
            building = building.setUri(url);

            request = building.build();
            
            request.addHeader("Content-Type", "application/json");
            // request.addHeader("Authorization", "Bearer " + token);
            
            HttpResponse response = httpclient.execute(request);
    
            int code = response.getStatusLine().getStatusCode();
            String bodyString = EntityUtils.toString(response.getEntity());

            //if (!bodyString.startsWith("["))
            //    bodyString = "[" + bodyString + "]";
            if (bodyString.isEmpty())
                bodyString = "{}";
                
            logger.logInfo("Retorno: " + bodyString);
            logger.logInfo("Code: " + code);
            
            Object resposta = new JSONTokener(bodyString).nextValue();
            
            JSONObject retorno = new JSONObject();
            Header[] headers = response.getAllHeaders();
            JSONArray headers_arr = new JSONArray();
            
            if (code == 200 || code == 201) {
                count = 0;
                success = true;
            }
        
            for (Header header : headers) {
                JSONObject header_obj = new JSONObject();
                header_obj.put(header.getName(), header.getValue());
                headers_arr.put(header_obj);
            }
            
            if (code >= 300 && code != 429 && code != 404) {
                logger.logInfo(String.format(" >>> OMIE: Erro executando [%s] em '%s' - Status Code=%s", metodo, url, code));
                logger.logInfo(String.format(" >>> OMIE: Response Headers: "));
                
                for (Header header : headers) {
                    logger.logInfo(String.format("   > %s=%s", header.getName(), header.getValue()));
                }  
                
                logger.logInfo(String.format(" >>> OMIE: Response Body: %s", bodyString));
                
                if (json != null)
                    logger.logInfo(String.format(" >>> OMIE: Request Body: %s", json.toString()));
    
                if (bodyString.contains("faultstring")) {
                    JSONObject bodyStr = new JSONObject(bodyString);
                    throw new RuntimeException(String.format(" >>> OMIE: %s: %s", response.getStatusLine().getReasonPhrase(), bodyStr.optString("faultstring")));
                }
    
                throw new RuntimeException(String.format(" >>> OMIE: Reason: %s", response.getStatusLine().getReasonPhrase()));
            }
        
            retorno.put("headers", headers_arr);
            retorno.put("response", resposta);
            retorno.put("code", code);
            retorno.put("success", (code == 200 || code == 201));
            
            success = true;
            
            return retorno;
        }
    
        return null;
    }
}
